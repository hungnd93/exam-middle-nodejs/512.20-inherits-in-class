// Tạo ra một class ConNguoi
class ConNguoi {
    // Hàm khởi tạo
    constructor(hoTen, ngaySinh, queQuan) {
        this.hoTen = hoTen || "unknown";
        this.ngaySinh = ngaySinh || "unknown";
        this.queQuan = queQuan || "unknown"
    }
    // phương thức
    getThongTin() {
        return `Họ tên: ${this.hoTen}, ngày sinh: ${this.ngaySinh}, quê quán: ${this.queQuan}`;
    }
}
export default ConNguoi;