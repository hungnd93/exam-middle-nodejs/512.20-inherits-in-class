import ConNguoi from "./con_nguoi.js";

// Tạo ra một class CongNhan kế thừa class ConNguoi
class CongNhan extends ConNguoi {
    // Hàm khởi tạo
    constructor(nganhNghe, noiLamViec, luong, hoTen, ngaySinh, queQuan) {
        super(hoTen, ngaySinh, queQuan);
        this.nganhNghe = nganhNghe || "unknown";
        this.noiLamViec = noiLamViec || "unknown";
        this.luong = luong || "unknown"
    }
    // phương thức
    getThongTinCongNhan() {
        return `${this.getThongTin()}, ngành nghề: ${this.nganhNghe}, nơi làm việc: ${this.noiLamViec}, lương: ${this.luong}`;
    }
}
export default CongNhan;