import HocSinh from "./hoc_sinh.js";

// tạo ra 1 class SinhVien kế thừa class HocSinh
class SinhVien extends HocSinh {
    constructor(chuyenNganh, MSSV, tenTruong, lop, soDienThoai, hoTen, ngaySinh, queQuan) {
        super(tenTruong, lop, soDienThoai, hoTen, ngaySinh, queQuan);
        this.chuyenNganh = chuyenNganh || "unknown";
        this.MSSV = MSSV || "unknown"
    }
    getThongTinSinhVien() {
        return `${this.getThongTinHocSinh()}, chuyên ngành: ${this.chuyenNganh}, MSSV: ${this.MSSV}`
    }
}
export default SinhVien;